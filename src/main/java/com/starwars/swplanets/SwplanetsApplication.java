package com.starwars.swplanets;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@EnableMongoAuditing
@EnableSpringDataWebSupport
@SpringBootApplication
public class SwplanetsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SwplanetsApplication.class, args);
	}

}
