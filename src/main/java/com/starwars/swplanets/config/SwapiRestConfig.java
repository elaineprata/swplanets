package com.starwars.swplanets.config;

import java.util.Optional;

import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.HttpClients;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class SwapiRestConfig {
	
	@Value("${swapi.endpoint}")
    private String endpoint;
    
    @Value("${swapi.connectiontimeout}")
    private int connectionTimeout;

    @Value("${swapi.sockettimeout}")
    private int socketTimeout;

    @Value("${swapi.requesttimeout}")
    private int requestTimeout;

    
    @Bean(name="swapiRestTemplate")
    public RestTemplate restTemplate() {

        RestTemplate restTemplate = new RestTemplate();
        
        return new RestTemplateBuilder()
	                .rootUri(endpoint)
	                .requestFactory(getRequestFactory().get().getClass())
	                .configure(restTemplate);

    }

    private Optional<ClientHttpRequestFactory> getRequestFactory() {

        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(connectionTimeout)
                .setSocketTimeout(socketTimeout)
                .setConnectionRequestTimeout(requestTimeout)
                .build();

        HttpClient httpClient = HttpClients.custom()
                .setDefaultRequestConfig(requestConfig)
                .build();

        ClientHttpRequestFactory requestFactory =
                new HttpComponentsClientHttpRequestFactory(httpClient);


        return Optional.ofNullable(requestFactory);

    }

}
