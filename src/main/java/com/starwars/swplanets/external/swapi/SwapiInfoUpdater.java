package com.starwars.swplanets.external.swapi;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.starwars.swplanets.planet.Planet;
import com.starwars.swplanets.planet.PlanetRepository;


@Component
public class SwapiInfoUpdater {
	
	Logger logger = LoggerFactory.getLogger(SwapiInfoUpdater.class);
	
	@Value("${swapi.addinfo.period:5}")
    private int cachePeriod;
	
	@Value("${swapi.addinfo.planetstoupdate:5}")
	private int planetsToUpdate;
	
	private PlanetRepository planetRepository;
	
	private SwapiPlanetService swapiPlanetService;
	
	@Autowired
	public SwapiInfoUpdater(PlanetRepository planetRepository, SwapiPlanetService swapiPlanetService) {
		this.planetRepository = planetRepository;
		this.swapiPlanetService = swapiPlanetService;
	}
	
	protected SwapiInfoUpdater(int cachePeriod,
							   int planetsToUpdate,
							   PlanetRepository planetRepository,
							   SwapiPlanetService swapiPlanetService) {
		this.cachePeriod = cachePeriod;
		this.planetsToUpdate = planetsToUpdate;
		this.planetRepository = planetRepository;
		this.swapiPlanetService = swapiPlanetService;
	}
	
	@Scheduled(cron = "${swapi.addinfo.cron}")
	public void updatePlanets() {
		
        List<Planet> planetsList = planetRepository.findByUpdatedAt(calculateDate(), PageRequest.of(0, planetsToUpdate));
		
        logger.info("Updating {} planets", planetsList.size());
        
        planetsList.stream().filter(p -> p.getExternalId() != null).forEach(planet -> {
        	logger.info("Updating planet {} films: {}", planet.getName(), planet.getFilmsCount());
			
			Optional<SwapiPlanet> oSwapiPlanet = swapiPlanetService.findPlanetById(planet.getExternalId());
			
			if (oSwapiPlanet.isPresent()) {
				SwapiPlanet sp = oSwapiPlanet.get();
				planet.setFilmsCount((sp.getFilms() != null)?sp.getFilms().size():0);
			
				planetRepository.save(planet);
			}
        });
        
	}
	
	private Date calculateDate() {
		LocalDate localDate = LocalDate.now().minusDays(cachePeriod);
		Date date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
		return date;
	}
}
