package com.starwars.swplanets.external.swapi;

import java.util.List;

public class SwapiPlanet {
	
	private String name;
	
	private String climate;
	
	private String terrain;
	
	private List<String> films;
	
	private String url;
	
	public SwapiPlanet() {
		
	}
	
	public SwapiPlanet(String name, String climate, String terrain, List<String> films, String url) {
		this.name = name;
		this.climate = climate;
		this.terrain = terrain;
		this.films = films;
		this.url = url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClimate() {
		return climate;
	}

	public void setClimate(String climate) {
		this.climate = climate;
	}

	public String getTerrain() {
		return terrain;
	}

	public void setTerrain(String terrain) {
		this.terrain = terrain;
	}

	public List<String> getFilms() {
		return films;
	}

	public void setFilms(List<String> films) {
		this.films = films;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "SwapiPlanet [name=" + name + ", climate=" + climate 
				+ ", terrain=" + terrain + ", films=" + films
				+ ", url=" + url + "]";
	}
	
}
