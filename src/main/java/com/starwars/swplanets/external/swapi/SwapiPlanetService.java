package com.starwars.swplanets.external.swapi;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class SwapiPlanetService {
	
	private static final String PLANET_RESOURCE_PATH = "/planets"; 

	private final RestTemplate restTemplate;
	
	@Autowired
	public SwapiPlanetService(@Qualifier("swapiRestTemplate")
							  RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	public Optional<SwapiPlanet> findPlanetById(String id) {
		SwapiPlanet p = callGetPlanet(PLANET_RESOURCE_PATH + "/" + id);
		return Optional.ofNullable(p);
	}
	
	public Optional<SwapiPlanet> findPlanetByName(String name) {
		
		SwapiResponse r = callGetResponse(PLANET_RESOURCE_PATH + "?search=" + name);
		Optional<SwapiPlanet> oPlanet = findPlanetInResults(name, r);
		
		if (!oPlanet.isPresent()) {
			while (r.getNext() != null) {
				r = callGetResponse(retrieveSearchString(r.getNext()));
				oPlanet = findPlanetInResults(name, r);
				if (oPlanet.isPresent()) {
					break;
				}
			}
		}
		
		return oPlanet;
	}

	private Optional<SwapiPlanet> findPlanetInResults(String name, SwapiResponse r) {
		
		return r.getResults().stream()
				.filter(sp -> (name.compareToIgnoreCase(sp.getName()) == 0))
				.findFirst();
	}

	private SwapiPlanet callGetPlanet(String search) {
		
		try {       	
        	ResponseEntity<SwapiPlanet> responseEntity = restTemplate.getForEntity(search, SwapiPlanet.class);
        	return responseEntity.getBody();
       	
        } catch (final Exception e) {
        	// TODO 404
            // final String errorBody = e.getResponseBodyAsString();
            throw e;
        }
	}

	private SwapiResponse callGetResponse(String search) {
		
		try {       	
        	ResponseEntity<SwapiResponse> responseEntity = restTemplate.getForEntity(search, SwapiResponse.class);
        	return responseEntity.getBody();
       	
        } catch (final Exception e) {
            // final String errorBody = e.getResponseBodyAsString();
            throw e;
        }
	}
	
	public String retrieveSwapId(String resourceUrl) {
		String[] path = resourceUrl.split(PLANET_RESOURCE_PATH);
		return path[1].replace("/", "");
	}
	
	private String retrieveSearchString(String endpoint) {
		String[] path = endpoint.split(PLANET_RESOURCE_PATH);
		return PLANET_RESOURCE_PATH + path[1];
	}
	
}