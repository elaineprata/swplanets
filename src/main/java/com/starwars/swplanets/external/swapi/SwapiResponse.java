package com.starwars.swplanets.external.swapi;

import java.util.List;

public class SwapiResponse {
	
	private Integer count;
	private String next;
	private String previous;
	private List<SwapiPlanet> results;
	
	public SwapiResponse() {
		
	}
	
	public SwapiResponse(Integer count, String next, String previous, List<SwapiPlanet> results) {
		this.count = count;
		this.next = next;
		this.previous = previous;
		this.results = results;
	}

	public Integer getCount() {
		return (count != null)?count:Integer.valueOf(0);
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public String getNext() {
		return next;
	}
	public void setNext(String next) {
		this.next = next;
	}
	public String getPrevious() {
		return previous;
	}
	public void setPrevious(String previous) {
		this.previous = previous;
	}
	public List<SwapiPlanet> getResults() {
		return results;
	}
	public void setResults(List<SwapiPlanet> results) {
		this.results = results;
	}
	@Override
	public String toString() {
		return "SwapiResponse [count=" + count + ", next=" + next 
				+ ", previous=" + previous + ", results=" + results
				+ "]";
	}
	
	
	
}