package com.starwars.swplanets.planet;

import java.util.Date;

import javax.validation.constraints.NotEmpty;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Planet {
	
	@Id
    private String id;
	
	@NotEmpty(message = "Please provide a name")
	@Indexed(unique=true)
	private String name;
	
	@NotEmpty(message = "Please provide climate")
	private String climate;
	
	@NotEmpty(message = "Please provide terrain")
	private String terrain;
	
	private Integer filmsCount = 0;
	
	private String externalId;
	
	@CreatedDate
	private Date createdAt;

	@LastModifiedDate
	private Date updatedAt;
	
	public Planet() {
		
	}
	
	public Planet(String id, String name, String climate, String terrain) {
		this.id = id;
		this.name = name;
		this.climate = climate;
		this.terrain = terrain;
	}

	public Planet(String id, String name, String climate, String terrain, String externalId) {
		this(id, name, climate, terrain);
		this.externalId = externalId;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClimate() {
		return climate;
	}

	public void setClimate(String climate) {
		this.climate = climate;
	}

	public String getTerrain() {
		return terrain;
	}

	public void setTerrain(String terrain) {
		this.terrain = terrain;
	}

	public Integer getFilmsCount() {
		return filmsCount;
	}

	public void setFilmsCount(Integer filmsCount) {
		this.filmsCount = filmsCount;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

}