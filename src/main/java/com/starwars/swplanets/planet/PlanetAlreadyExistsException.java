package com.starwars.swplanets.planet;

public class PlanetAlreadyExistsException extends RuntimeException {
	
	private static final long serialVersionUID = 4033560122543397711L;

	public PlanetAlreadyExistsException(String name) {
		super("Planet already exists: " + name);
	}

}
