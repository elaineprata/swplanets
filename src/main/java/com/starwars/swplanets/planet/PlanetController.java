package com.starwars.swplanets.planet;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("planets")
public class PlanetController {
	
	@Autowired
	PlanetService planetService;

	@GetMapping("/{id}")
	public ResponseEntity<?> get(@PathVariable final String id) {
		
		Planet planet = planetService.findById(id);
		return new ResponseEntity<Planet>(planet, HttpStatus.OK);
		
	}

	@GetMapping("/")
	public ResponseEntity<?> listAll(
								@RequestParam(required = false)
								final String name,
								@PageableDefault(size = 20, direction = Sort.Direction.ASC, sort = "name")
								Pageable p,
								final PagedResourcesAssembler<Planet> pagedResourcesAssembler) {
		
		if (!StringUtils.isEmpty(name)) {
			Page<Planet> page = planetService.findByName(name, p);
			return new ResponseEntity<>(pagedResourcesAssembler.toResource(page), HttpStatus.OK);
		} else {
			Page<Planet> page = planetService.findAll(p);
			return new ResponseEntity<>(pagedResourcesAssembler.toResource(page), HttpStatus.OK);
		}
		
	}

	@PostMapping("/")
    public ResponseEntity<Planet> post(
                                    @Valid
                                    @RequestBody
                                    final Planet request) {
		Planet planet = planetService.createPlanet(request);
		return new ResponseEntity<Planet>(planet, HttpStatus.CREATED);
	}

	@DeleteMapping("/{id}")
    public ResponseEntity<Planet> delete(@PathVariable final String id) {
		planetService.deletePlanet(id);
		return new ResponseEntity<Planet>(HttpStatus.NO_CONTENT);
	}

}
