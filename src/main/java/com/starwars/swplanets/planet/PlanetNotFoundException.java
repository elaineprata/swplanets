package com.starwars.swplanets.planet;

public class PlanetNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 2845940838103970661L;

	public PlanetNotFoundException(String id) {
        super("Planet not found: " + id);
    }

}