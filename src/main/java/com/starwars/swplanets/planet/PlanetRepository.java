package com.starwars.swplanets.planet;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PlanetRepository extends PagingAndSortingRepository<Planet, String> {
	
	public Page<Planet> findByNameStartsWith(String name, Pageable pageable);
	
	@Query(value = "{'updatedAt': {'$lt': ?0}}", sort = "{updatedAt : 1}")
	List<Planet> findByUpdatedAt(Date date, Pageable pageable);

}