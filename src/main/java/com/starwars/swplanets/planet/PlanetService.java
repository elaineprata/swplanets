package com.starwars.swplanets.planet;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.starwars.swplanets.external.swapi.SwapiPlanet;
import com.starwars.swplanets.external.swapi.SwapiPlanetService;


@Service
public class PlanetService {
	
	private PlanetRepository planetRepository;
	
	private SwapiPlanetService swapiService;
	
	@Autowired
	public PlanetService(PlanetRepository planetRepository, SwapiPlanetService swapiService) {
		this.planetRepository = planetRepository;
		this.swapiService = swapiService;
	}
	
	public Planet findById(String id) {
		Planet planet = planetRepository.findById(id)
				.orElseThrow(() -> new PlanetNotFoundException(id));

		return planet;
	}

	public Page<Planet> findByName(String name, Pageable p) {
		return planetRepository.findByNameStartsWith(name, p);
	}
	
	public Page<Planet> findAll(Pageable p) {
		return planetRepository.findAll(p);
	}

	public Planet createPlanet(Planet planet) {
		
		Optional<SwapiPlanet> oSwapiPlanet = swapiService.findPlanetByName(planet.getName());
		if (oSwapiPlanet.isPresent()) {
			SwapiPlanet swapiPlanet = oSwapiPlanet.get();
			planet.setFilmsCount((swapiPlanet.getFilms() != null)?swapiPlanet.getFilms().size():0);
			
			String externalId = swapiService.retrieveSwapId(swapiPlanet.getUrl());
			planet.setExternalId(externalId);			
		}
		
		try {
			return planetRepository.save(planet);
		} catch (DuplicateKeyException dke) {
			throw new PlanetAlreadyExistsException(planet.getName());
		}
		
	}
	
	public void deletePlanet(String id) {
		Planet planet = planetRepository.findById(id)
		.orElseThrow(() -> new PlanetNotFoundException(id));
		planetRepository.delete(planet);
	}

}