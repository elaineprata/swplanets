package com.starwars.swplanets;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.starwars.swplanets.external.swapi.SwapiInfoUpdaterTest;
import com.starwars.swplanets.external.swapi.SwapiPlanetServiceTest;
import com.starwars.swplanets.planet.PlanetControllerTest;
import com.starwars.swplanets.planet.PlanetServiceTest;


@RunWith(Suite.class)
@SuiteClasses({PlanetServiceTest.class, SwapiPlanetServiceTest.class, SwapiInfoUpdaterTest.class, PlanetControllerTest.class})
public class SuiteTest {

}
