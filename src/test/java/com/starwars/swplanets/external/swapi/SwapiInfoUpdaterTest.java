package com.starwars.swplanets.external.swapi;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.AdditionalAnswers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.PageRequest;

import com.starwars.swplanets.planet.Planet;
import com.starwars.swplanets.planet.PlanetRepository;

public class SwapiInfoUpdaterTest {
	
	@Mock
	private PlanetRepository planetRepository;
	
	@Mock
	private SwapiPlanetService swapiPlanetService;
	
	private SwapiInfoUpdater swapiInfoUpdater;
	
	@Before
    public void before() throws Exception {
        MockitoAnnotations.initMocks(this);
        
        swapiInfoUpdater = new SwapiInfoUpdater(2, 2, planetRepository, swapiPlanetService);
    }
	
	@Test
	public void updatePlanets() {
		
		List<Planet> planetsList = new ArrayList<Planet>();
		planetsList.add(new Planet("id", "name", "climate", "terrain", "externalId1"));
		planetsList.add(new Planet("id", "name", "climate", "terrain", "externalId2"));
		
		when(planetRepository.findByUpdatedAt(any(Date.class), any(PageRequest.class)))
			.thenReturn(planetsList);
		
		when(swapiPlanetService.findPlanetById(any(String.class)))
			.thenReturn(Optional.of(new SwapiPlanet("PlanetOne", "climate", "terrain", Arrays.asList("film1", "film2"), "url")))
			.thenReturn(Optional.ofNullable(null));
			
		when(planetRepository.save(any(Planet.class))).then(AdditionalAnswers.returnsFirstArg());
		
		swapiInfoUpdater.updatePlanets();
			
		verify(planetRepository, times(1)).save(any(Planet.class));
		
	}

	@Test
	public void updatePlanets_noUpdate() {
		
		List<Planet> planetsList = new ArrayList<Planet>();
		planetsList.add(new Planet("id", "name", "climate", "terrain", "externalId1"));
		planetsList.add(new Planet("id", "name", "climate", "terrain", "externalId2"));
		
		when(planetRepository.findByUpdatedAt(any(Date.class), any(PageRequest.class)))
			.thenReturn(planetsList);
		
		when(swapiPlanetService.findPlanetById(any(String.class)))
			.thenReturn(Optional.ofNullable(null))
			.thenReturn(Optional.ofNullable(null));
			
		when(planetRepository.save(any(Planet.class))).then(AdditionalAnswers.returnsFirstArg());
		
		swapiInfoUpdater.updatePlanets();
			
		verify(planetRepository, never()).save(any(Planet.class));
		
	}

}
