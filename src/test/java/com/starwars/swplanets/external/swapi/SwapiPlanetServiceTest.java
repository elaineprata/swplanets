package com.starwars.swplanets.external.swapi;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class SwapiPlanetServiceTest {

	@Mock
    private RestTemplate restTemplate;
	
	private SwapiPlanetService swapiPlanetService;
	
	@Before
    public void before() throws Exception {
        MockitoAnnotations.initMocks(this);
        
        swapiPlanetService = new SwapiPlanetService(restTemplate);
    }
	
	@Test
	public void findPlanetById_success() {
		
		SwapiPlanet expectedPlanet = new SwapiPlanet("name", "climate", "terrain", Arrays.asList("film1", "film2"), "url");
				
		when(restTemplate.getForEntity("/planets/id", SwapiPlanet.class))
			.thenReturn(new ResponseEntity<SwapiPlanet>(expectedPlanet, HttpStatus.OK));

		Optional<SwapiPlanet> osp = swapiPlanetService.findPlanetById("id");
		
		assertTrue(osp.isPresent());
        assertEquals(expectedPlanet.getUrl(), osp.get().getUrl());
        assertEquals(expectedPlanet.getName(), osp.get().getName());
        assertEquals(expectedPlanet.getClimate(), osp.get().getClimate());
	}
	
	@Test
	public void findPlanetByName_success() {
		
		List<SwapiPlanet> firstPageResults = new ArrayList<SwapiPlanet>();
		firstPageResults.add(new SwapiPlanet("PlanetOne", "climate", "terrain", Arrays.asList("film1", "film2"), "url"));
		firstPageResults.add(new SwapiPlanet("PlanetTwo", "climate", "terrain", Arrays.asList("film1", "film2"), "url"));
		SwapiResponse firstSwapiResponse = new SwapiResponse(Integer.valueOf(10), "https://swapi.co/api/planets/?page=2", null, firstPageResults);
		
		List<SwapiPlanet> secondPageResults = new ArrayList<SwapiPlanet>();
		secondPageResults.add(new SwapiPlanet("PlanetThree", "climate", "terrain", Arrays.asList("film1", "film2"), "url"));
		secondPageResults.add(new SwapiPlanet("PlanetFour", "climate 4", "terrain", Arrays.asList("film1", "film2"), "url"));
		SwapiResponse secondSwapiResponse = new SwapiResponse(Integer.valueOf(10), "https://swapi.co/api/planets/?page=3", "https://swapi.co/api/planets/?page=2", secondPageResults);
		
		when(restTemplate.getForEntity(any(String.class), any(Class.class)))
			.thenReturn(new ResponseEntity<SwapiResponse>(firstSwapiResponse, HttpStatus.OK))
			.thenReturn(new ResponseEntity<SwapiResponse>(secondSwapiResponse, HttpStatus.OK));

		Optional<SwapiPlanet> osp = swapiPlanetService.findPlanetByName("PlanetFour");
		
		assertTrue(osp.isPresent());
        assertEquals("url", osp.get().getUrl());
        assertEquals("PlanetFour", osp.get().getName());
        assertEquals("climate 4", osp.get().getClimate());
	}

}
