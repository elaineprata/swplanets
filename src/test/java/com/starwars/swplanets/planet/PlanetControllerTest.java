package com.starwars.swplanets.planet;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.starwars.swplanets.SwplanetsApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SwplanetsApplication.class)
@WebAppConfiguration
public class PlanetControllerTest {
	
	protected MockMvc mockMvc;
	   
	@Autowired
	WebApplicationContext webApplicationContext;
	
	@MockBean
    private PlanetService planetService;

    
    @Before
    public void setUp() {
    	mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }
	
	@Test
    public void testPost_success() throws Exception {

        final String json = "{\n" + 
        		"	\"name\": \"Utapau\",\n" + 
        		"	\"climate\": \"Climate\",\n" + 
        		"	\"terrain\": \"Terrain\"\n" + 
        		"}";

        Planet result = new Planet("1234", "Planet01", "Climate", "Terrain");
        
        when(planetService.createPlanet(any())).thenReturn(result);

        final ResultActions resultActions = mockMvc.perform(post("/planets/")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON));

        resultActions
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value("1234"))
                .andExpect(jsonPath("$.name").value("Planet01"));
    }

	@Test
    public void testPost_409() throws Exception {

        final String json = "{\n" + 
        		"	\"name\": \"Utapau\",\n" + 
        		"	\"climate\": \"Climate\",\n" + 
        		"	\"terrain\": \"Terrain\"\n" + 
        		"}";

        when(planetService.createPlanet(any())).thenThrow(new PlanetAlreadyExistsException(""));

        final ResultActions resultActions = mockMvc.perform(post("/planets/")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON));

        resultActions.andExpect(status().isConflict());
    }

	@Test
	public void testGet_byId_success() throws Exception {
		
		Planet result = new Planet("1234", "Planet01", "Climate", "Terrain");

        when(planetService.findById(any())).thenReturn(result);

        final ResultActions resultActions = mockMvc.perform(get("/planets/{id}", "1"));

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1234"))
                .andExpect(jsonPath("$.name").value("Planet01"));

	}

	@Test
	public void testGet_byId_404() throws Exception {
		
        when(planetService.findById(any())).thenThrow(new PlanetNotFoundException(""));

        final ResultActions resultActions = mockMvc.perform(get("/planets/{id}", "1"));

        resultActions.andExpect(status().isNotFound());

	}
	
	@Test
    public void testGet_list_success() throws Exception {

		Planet planet1 = new Planet("1234", "Planet01", "Climate", "Terrain");
		Planet planet2 = new Planet("1235", "Planet02", "Climate", "Terrain");

        List<Planet> planetList = new ArrayList<>();
        planetList.add(planet1);
        planetList.add(planet2);

        Page<Planet> planetPage = new PageImpl<>(planetList);

        when(planetService.findAll(any(Pageable.class))).thenReturn(planetPage);

        final ResultActions resultActions = mockMvc.perform(get("/planets/"));

        resultActions.andExpect(status().isOk());
    }

	
	@Test
	public void testDelete_success() throws Exception {
		
		doNothing().when(planetService).deletePlanet(any());

        final ResultActions resultActions = mockMvc.perform(delete("/planets/{id}", "1"));

        resultActions.andExpect(status().isNoContent());

	}

	@Test
	public void testDelete_404() throws Exception {
		
		doThrow(new PlanetNotFoundException("")).when(planetService).deletePlanet(any());

        final ResultActions resultActions = mockMvc.perform(delete("/planets/{id}", "1"));

        resultActions.andExpect(status().isNotFound());

	}
	
}