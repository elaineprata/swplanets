package com.starwars.swplanets.planet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.AdditionalAnswers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.starwars.swplanets.external.swapi.SwapiPlanet;
import com.starwars.swplanets.external.swapi.SwapiPlanetService;

public class PlanetServiceTest {
	
	@Mock
    private PlanetRepository planetRepository;
	
	@Mock
	private SwapiPlanetService swapiPlanetService;

	private PlanetService planetService;
	
	@Before
    public void before() throws Exception {
        MockitoAnnotations.initMocks(this);
        
        planetService = new PlanetService(planetRepository, swapiPlanetService);
    }

	@Test
	public void findById_success() {
		
		Planet expectedPlanet = new Planet("id", "name", "climate", "terrain");
		
		when(planetRepository.findById(any(String.class))).thenReturn(Optional.of(expectedPlanet));

		Planet planet = planetService.findById("id");

        assertEquals(expectedPlanet.getId(), planet.getId());
        assertEquals(expectedPlanet.getName(), planet.getName());
        assertEquals(expectedPlanet.getClimate(), planet.getClimate());
	}

	@Test(expected = PlanetNotFoundException.class)
	public void findById_notFound() {
		when(planetRepository.findById(any(String.class))).thenReturn(Optional.ofNullable(null));

		planetService.findById("id");
	}
	
	@Test
	public void findByName_success() {
		
		Planet expectedPlanet = new Planet("id", "name", "climate", "terrain");
		List<Planet> planetList = new ArrayList<Planet>();
		planetList.add(expectedPlanet);
		Page<Planet> pimpl = new PageImpl<Planet>(planetList);
		
		Pageable p = PageRequest.of(0, 10);
		when(planetRepository.findByNameStartsWith("name", p)).thenReturn(pimpl);
		
		Page<Planet> resultPage = planetService.findByName("name", p);
		List<Planet> resultPlanetList = resultPage.getContent();
		
		assertNotNull(resultPlanetList);
		assertEquals(1, resultPlanetList.size());
        assertEquals(expectedPlanet.getId(), resultPlanetList.get(0).getId());
        assertEquals(expectedPlanet.getName(), resultPlanetList.get(0).getName());
        assertEquals(expectedPlanet.getClimate(), resultPlanetList.get(0).getClimate());
	}
	
	@Test
	public void findAll_success() {
		
		Planet expectedPlanet = new Planet("id", "name", "climate", "terrain");
		List<Planet> planetList = new ArrayList<Planet>();
		planetList.add(expectedPlanet);
		Page<Planet> pimpl = new PageImpl<Planet>(planetList);
		
		Pageable p = PageRequest.of(0, 10);
		when(planetRepository.findAll(p)).thenReturn(pimpl);
		
		Page<Planet> resultPage = planetService.findAll(p);
		List<Planet> resultPlanetList = resultPage.getContent();
		
		assertNotNull(resultPlanetList);
		assertEquals(1, resultPlanetList.size());
        assertEquals(expectedPlanet.getId(), resultPlanetList.get(0).getId());
        assertEquals(expectedPlanet.getName(), resultPlanetList.get(0).getName());
        assertEquals(expectedPlanet.getClimate(), resultPlanetList.get(0).getClimate());
	}
	
	@Test
	public void createPlanet_success() {
		
		Planet planet = new Planet();
		planet.setName("Planet A");
		
		SwapiPlanet sp = new SwapiPlanet("Planet A", 
										"Climate",
										"Terrain",
										Arrays.asList("film1", "film2"), 
										"url");
		
		when(swapiPlanetService.findPlanetByName("Planet A")).thenReturn(Optional.of(sp));
		when(swapiPlanetService.retrieveSwapId(any(String.class))).thenReturn("swapiid");
		
		when(planetRepository.save(any(Planet.class))).then(AdditionalAnswers.returnsFirstArg());
		
		planet = planetService.createPlanet(planet);
		
		assertEquals(2, planet.getFilmsCount().intValue());
		assertEquals("swapiid", planet.getExternalId());
		
	}

	@Test
	public void createPlanet_success_planetNotFoundOnSwapi() {
		
		Planet planet = new Planet();
		planet.setName("Planet A");
		
		SwapiPlanet sp = null;
		
		when(swapiPlanetService.findPlanetByName("Planet A")).thenReturn(Optional.ofNullable(sp));
		when(swapiPlanetService.retrieveSwapId(any(String.class))).thenReturn("swapiid");
		
		when(planetRepository.save(any(Planet.class))).then(AdditionalAnswers.returnsFirstArg());
		
		planet = planetService.createPlanet(planet);
		
		assertEquals(0, planet.getFilmsCount().intValue());
	}
	
	@Test
	public void deletePlanet_success() {
		
		Planet expectedPlanet = new Planet("id", "name", "climate", "terrain");
		
		when(planetRepository.findById(any(String.class))).thenReturn(Optional.of(expectedPlanet));
		Mockito.doNothing().when(planetRepository).delete(any(Planet.class));

		planetService.deletePlanet("id");
		
		verify(planetRepository, times(1)).delete(any(Planet.class));
	}

	@Test(expected = PlanetNotFoundException.class)
	public void deletePlanet_notFound() {
		when(planetRepository.findById(any(String.class))).thenReturn(Optional.ofNullable(null));

		planetService.deletePlanet("id");
	}
	
}